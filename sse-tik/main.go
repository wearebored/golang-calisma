package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"
)

var data = 0

type Veri struct{
	Data int `json:"data"`
}
type GelenVeri struct{
	Data string `json:"data"`
}
// var sayı = 0

func main()  {
	fmt.Println("data")

	
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/sse",dinleyici)
	http.HandleFunc("/tik",tiklama)
	
	http.ListenAndServe(":8080", nil)
}


func tiklama(w http.ResponseWriter,r *http.Request)  {
	
	// if sayı%2==0{
	// 	time.Sleep(time.Second*1)
	// 	sayı++
	// }else{
	// 	time.Sleep(time.Second*2)
	// 	sayı++
	// }
	data++
	fmt.Println("tıklandı")
}

func dinleyici(w http.ResponseWriter,r *http.Request)  {
		w.Header().Set("Content-Type", "text/event-stream")
        w.Header().Set("Cache-Control", "no-cache")
        w.Header().Set("Connection", "keep-alive")
		kanal := make(chan bool)

		// -----------gelen verinin işlenmesi--------
		gelenveri,err:=io.ReadAll(r.Body)
		veristring:=string(gelenveri)
		gelendata := GelenVeri{}
		json.Unmarshal([]byte(veristring), &gelendata)
		// ------------------

		süre :=true
		go func() {
			for süre {
				if gelendata.Data!=strconv.Itoa(data){
					kanal<-true
				}
				time.Sleep(time.Millisecond*200)
				
			}
			kanal<-true
		}()
		go func() {
			time.Sleep(time.Second*5)
			süre=false
			
		}()

	<-kanal

	// ------------gönderilecek veri---------	
	veri:=Veri{
		Data: data,
	}

	jsonData, err := json.Marshal(veri)
	if err != nil {
		fmt.Println(err)
		return
	}


    fmt.Fprintf(w, string(jsonData))
    w.(http.Flusher).Flush()
	// fmt.Println(data)
	
}