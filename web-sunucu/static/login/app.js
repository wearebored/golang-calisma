let kullanıcı;
let sifre;

document.getElementById("kullanici").onchange = function () {
  kullanıcı = document.getElementById("kullanici").value;
};
document.getElementById("sifre").onchange = function () {
  sifre = document.getElementById("sifre").value;
};
document.getElementById("giris").onclick = async () => {
  console.log(kullanıcı);
  console.log(sifre);
  document.getElementById("kullanici").value = "";
  document.getElementById("sifre").value = "";

  data = await fetch("http://localhost:8080/login/data", {
    method: "POST",
    body: JSON.stringify({
      kullanıcı,
      sifre,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((json) => json);

  if (data.message == "okey") {
    document.getElementById("bilgi").innerText = "Giriş tamamlandı";
    setTimeout(() => {
      window.location.href = "http://localhost:8080/dashboard";
    }, 5000);
  } else {
    document.getElementById("bilgi").innerText = "Hatalı giriş bilgileri.";
  }

  
};
