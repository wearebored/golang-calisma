package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)
type Veri struct {
  Kullanici string `json:"kullanıcı"`
  Sifre string  `json:"sifre"`
}

var Kullanici string
var Sifre string



func main()  {
	
	

    http.HandleFunc("/", home) 
    http.HandleFunc("/login/", login) 
    http.HandleFunc("/register/", register) 
    http.HandleFunc("/register/data", registerData) 
    http.HandleFunc("/login/data", loginData) 
	http.HandleFunc("/dashboard",dashboard)
	

    fmt.Printf("Starting server at port 8080\n")
    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatal(err)
    }


}

func home(w http.ResponseWriter,r *http.Request)  {
	const homehtml =`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div>
      <h1>Welcome</h1>
      <button id="register">Register</button>
      <button id="login">Login</button>
    </div>
    <script>
      document.getElementById("register").onclick = function () {
        window.location.href = "http://localhost:8080/register";
      };
      document.getElementById("login").onclick = function () {
        window.location.href = "http://localhost:8080/login";
      };
    </script>
  </body>
</html>
`
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprint(w, homehtml)
}
func login(w http.ResponseWriter,r *http.Request)  {
	const loginhtml =`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div>
      <!-- <form method="POST" action="/form">
        <label>Kullanıcı adı</label><input name="name" type="text" value="" />
        <label>Şifre</label><input name="sifre" type="text" value="" />
        <input type="submit" value="submit" />
      </form> -->
      <p>Kullanıcı adı</p>
      <input id="kullanici" type="text" />
      <p>Şifre</p>
      <input id="sifre" type="text" />
      <br />
      <br />
      <button id="giris">Giriş</button>
	   <p id="bilgi" ></p>
    </div>
    <script>
      let kullanıcı;
let sifre;

document.getElementById("kullanici").onchange = function () {
  kullanıcı = document.getElementById("kullanici").value;
};
document.getElementById("sifre").onchange = function () {
  sifre = document.getElementById("sifre").value;
};
document.getElementById("giris").onclick = async () => {
  console.log(kullanıcı);
  console.log(sifre);
  document.getElementById("kullanici").value = "";
  document.getElementById("sifre").value = "";

  data = await fetch("http://localhost:8081/login/data", {
    method: "POST",
    body: JSON.stringify({
      kullanıcı,
      sifre,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((json) => json);

  if (data.message == "okey") {
    document.getElementById("bilgi").innerText = "Giriş tamamlandı";
    setTimeout(() => {
      window.location.href = "http://localhost:8081/dashboard";
    }, 5000);
  } else {
    document.getElementById("bilgi").innerText = "Hatalı giriş bilgileri.";
  }

  
};

    </script>
  </body>
</html>`
	fmt.Println(Kullanici,Sifre)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprint(w, loginhtml)
}


func register(w http.ResponseWriter,r *http.Request)  {
	const registerhtml =`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div>
      <!-- <form method="POST" action="/form">
        <label>Kullanıcı adı</label><input name="name" type="text" value="" />
        <label>Şifre</label><input name="sifre" type="text" value="" />
        <input type="submit" value="submit" />
      </form> -->
      <p>Kullanıcı adı</p>
      <input id="kullanici" type="text" />
      <p>Şifre</p>
      <input id="sifre" type="text" />
      <br />
      <br />
      <button id="giris">Kayıt Ol</button>
	  <p id="bilgi" ></p>
    </div>
    <script>
      let kullanıcı;
let sifre;

document.getElementById("kullanici").onchange = function () {
  kullanıcı = document.getElementById("kullanici").value;
};
document.getElementById("sifre").onchange = function () {
  sifre = document.getElementById("sifre").value;
};
document.getElementById("giris").onclick = async ()=> {
  console.log(kullanıcı);
  console.log(sifre);
  document.getElementById("kullanici").value = "";
  document.getElementById("sifre").value = "";


     data = await fetch("http://localhost:8081/register/data", {
       method: "POST",
       body: JSON.stringify({
         kullanıcı,
         sifre,
       }),
       headers: {
         "Content-type": "application/json; charset=UTF-8",
       },
     })
       .then((response) => response.json())
       .then((json) => json);

      if (data.message=="okey") {
      document.getElementById("bilgi").innerText="Kayıt tamamlandı."
      setTimeout(() => {
        window.location.href = "http://localhost:8081/login";
      }, 5000);
    }else{
      document.getElementById("bilgi").innerText = "Hatalı kayıt";
    }
};

    </script>
  </body>
</html>`
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprint(w, registerhtml)
}

func registerData(w http.ResponseWriter,r *http.Request)  {
	veri,err:=io.ReadAll(r.Body)
	veristring:=string(veri)
	data := Veri{}
    json.Unmarshal([]byte(veristring), &data)
	if err ==nil{
		Kullanici=data.Kullanici
		Sifre=data.Sifre
		resp := make(map[string]string)
		resp["message"] = "okey"
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
	}else{
		resp := make(map[string]string)
		resp["message"] = "error"
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
	}
	
    

}

func loginData(w http.ResponseWriter,r *http.Request)  {
	veri,err:=io.ReadAll(r.Body)
	veristring:=string(veri)
	data := Veri{}
    json.Unmarshal([]byte(veristring), &data)
	if err ==nil{
		if data.Kullanici==Kullanici&&data.Sifre==Sifre {
			resp := make(map[string]string)
			resp["message"] = "okey"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				log.Fatalf("Error happened in JSON marshal. Err: %s", err)
			}
			w.Write(jsonResp)
		}else{
			resp := make(map[string]string)
			resp["message"] = "error"
			jsonResp, err := json.Marshal(resp)
			if err != nil {
				log.Fatalf("Error happened in JSON marshal. Err: %s", err)
			}
			w.Write(jsonResp)
		}
		
	}else{
		resp := make(map[string]string)
		resp["message"] = "error"
		jsonResp, err := json.Marshal(resp)
		if err != nil {
			log.Fatalf("Error happened in JSON marshal. Err: %s", err)
		}
		w.Write(jsonResp)
	}

}
func dashboard(w http.ResponseWriter,r *http.Request)  {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprint(w, "Dashboard")
	
}












// func admin(w http.ResponseWriter, r *http.Request) {
// // fileServer := http.FileServer(http.Dir("./static"))
// 	fmt.Println(Kullanici)
	
// 	var veriler Veri
// 	body, _:= io.ReadAll(r.Body)
// 	fmt.Println(string(body))
	
// 	json.Unmarshal([]byte(string(body)), &veriler)

// 	fmt.Println(veriler.Kullanici)
// 	fmt.Println(veriler.Sifre)

// 	// w.Header().Set("Content-Type", "text/html; charset=utf-8")
// 	// get a payload 
// 	// w.WriteHeader(http.StatusCreated)
// 	// w.Header().Set("Content-Type", "application/json")
// 	resp := make(map[string]string)
// 	resp["message"] = "Status Createddawdw"
// 	jsonResp, err := json.Marshal(resp)
// 	if err != nil {
// 		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
// 	}
// 	w.Write(jsonResp)







// 	p := []string{"adw","wfdawd","fdawd"}
// 	json.NewEncoder(w).Encode(p)
   
//    w.Write([]byte("<h1>Welcome to my web server!</h1>"))
// }