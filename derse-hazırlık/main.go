package main

import (
	"fmt"
	"sync"
	"time"
)

func pinger(c chan string) {
  for i := 0; ; i++ {
    c <- "ping"
  }
}

func printer(c chan string) {
  for {
    msg := <- c
    fmt.Println(msg)
    time.Sleep(time.Second * 1)
	break
  }
  
}

type intLock struct {
	val int
	sync.Mutex
}

func (n *intLock) isEven() bool {
	return n.val%2 == 0
}

func main() {
//   var c chan string = make(chan string)
// fmt.Println("1")
//   go pinger(c)
// fmt.Println("2")

//   go printer(c)
//   <-c
// fmt.Println("3")

// <-c
//   fmt.Println("son")
ch:=make(chan int,2)
n := &intLock{val: 0}
go func() {
	n.Lock()
	nIsEven := n.isEven()
	time.Sleep(5 * time.Millisecond)
	if nIsEven {
		fmt.Println(n.val, " is even")
		// mutex is never unlocked
		ch<-1
		return
	}
	fmt.Println(n.val, "is odd")
	ch<-2
}()
	go func() {
		time.Sleep(time.Millisecond*2)
	n.val++
	fmt.Println(n.val)
	ch<-3
	}()

<-ch
n.val++
fmt.Println(n.val)


			fmt.Println(" yol4")


}