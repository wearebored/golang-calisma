package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

)


func main()  {
	zarSonuc1:=make(chan int)
	zarSonuc2:=make(chan int)
	geridon:=make(chan bool,2)
	oyuncu:=make(chan bool)
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		zarat(zarSonuc1,1,oyuncu)
		for  {
			<-geridon
			zarat(zarSonuc1,1,oyuncu)
		}
		
		
	}()
	
	go func() {
		zarat(zarSonuc2,2,oyuncu)
		for  {
			time.Sleep(time.Second*2)
			<-geridon
			
			zarat(zarSonuc2,2,oyuncu)
		}
	}()
	go func() {
		hakem(zarSonuc1,zarSonuc2,geridon,&wg)
	}()

	wg.Wait()
}


func hakem(zarSonuc1 chan int,zarSonuc2 chan int,geridon chan bool,wg *sync.WaitGroup)  {
	zartoplam1:=0
	zartoplam2:=0
	for{
		zar1:=<-zarSonuc1
		zar2:=<-zarSonuc2
		zartoplam1=zartoplam1+zar1
		zartoplam2=zartoplam2+zar2

		fmt.Println("v---------v")
		fmt.Println(zartoplam1)
		fmt.Println(zartoplam2)
		fmt.Println("^---------^")
		
		if zartoplam1>=20||zartoplam2>=20 {
			if zartoplam1>=20&&zartoplam2>=20{
				fmt.Println("2 oyuncununda puanları 20 üzeri")
				// if zartoplam1>zartoplam2{
				// 	fmt.Println("En yüksek sayıyı 1. oyuncu aldı.")
				// 	break
				//  	wg.Done()
				// }else if zartoplam2>zartoplam1 {
				// 	fmt.Println("En yüksek sayıyı 2. oyuncu aldı.")
				// 	break
				//  	wg.Done()
				// }
				fmt.Println("Oyun tekrar başlıyor...")
				zartoplam1=0
				zartoplam2=0
				geridon<-true
				geridon<-true
				geridon<-true

			}else if zartoplam1>=20{
				fmt.Println("1. oyuncu kazandı. Puan:",zartoplam1)
				// wg.Done()
				break
			} else if zartoplam2>=20{
				fmt.Println("2. oyuncu kazandı. Puan:",zartoplam2)
				// wg.Done()
				break
			}
			
		}else{
			geridon<-true
			geridon<-true
		}

		
	}
	wg.Done()
}


func zarat(zs chan int,sn int,oyuncu chan bool ) {
	
	if sn==1{
	say:=rand.Intn(6)+1
  	fmt.Printf("%o.oyuncu zar sonucu: %o\n",sn,say)
	time.Sleep(time.Second*time.Duration(sn) )
	zs<-say
	oyuncu<-true
	}else if sn ==2{
		<-oyuncu
		say:=rand.Intn(6)+1
  		fmt.Printf("%o.oyuncu zar sonucu: %o\n",sn,say)
		time.Sleep(time.Second*time.Duration(sn) )
		zs<-say
	}
  	
		
}