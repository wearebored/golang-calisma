package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

)


var htmlHomeText=`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <button id="login">Login</button>
    <br>
    <button id="register">Register</button>

    <script>
        document.getElementById("login").onclick=()=>{
             window.location.href = "http://localhost:`+port+`/login"
        }
        document.getElementById("register").onclick=()=>{
             window.location.href = "http://localhost:`+port+`/register"
        }
    </script>
    
</body>
</html>`
var htmlLoginText=`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
		<button id="register">Register</button>
		
        <button id="home" >Home</button>
		<br>
    <form action="/login" method="post">
        <label  for="username">Username</label>
		<br>
        <input value="" type="text" name="username" id="username">
        <br>
        <label for="password">Password</label>
		<br>
        <input value="" type="password" name="password" id="password">
        <br>
        <button type="submit">Login</button>
    </form>
	
	<script>
        document.getElementById("register").onclick=()=>{
             window.location.href = "http://localhost:`+port+`/register"
        }
        document.getElementById("home").onclick=()=>{
             window.location.href = "http://localhost:`+port+`"
        }
    </script>
</body>
</html>`
var htmlRegisterText=`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
		<button id="login">Login</button>
		
        <button id="home" >Home</button>
		<br>
    <form id="registerform" action="/register" method="post">
        <label  for="username">Username</label>
		<br>
        <input value="" type="text" name="username" id="username">
        <br>
        <label for="password">Password</label>
		<br>
        <input value="" type="password" name="password" id="password">
        <br>
        <button type="submit">Register</button>
		
    </form>
	
        
	<script>
        document.getElementById("login").onclick=()=>{
             window.location.href = "http://localhost:`+port+`/login"
        }
        document.getElementById("home").onclick=()=>{
             window.location.href = "http://localhost:`+port+`"
        }
    </script>
</body>
</html>`

var htmlRegisterkayitText=`<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <p>Kayıt işleminiz tamamlandı.</p>
  <p>Giriş sayfasına yönlendiriliyorsunuz...</p>

  <script>
    setTimeout(()=>(window.location.href = "http://localhost:`+port+`/login"), 3000);
    
  </script>
</body>
</html>`

var username=""
var password=""
var port ="8080"

func main()  {
	http.HandleFunc("/",home)
	http.HandleFunc("/login",login)
	http.HandleFunc("/register",register)
	http.HandleFunc("/register/kayit",registerkayit)

	fmt.Printf("Starting server at port 8080\n")
    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatal(err)
    }
}

func home(w http.ResponseWriter, r *http.Request)  {
	htmlText(w,htmlHomeText)

}

func login(w http.ResponseWriter, r *http.Request)  {

	htmlText(w,htmlLoginText)
	
	if err := r.ParseForm(); err != nil {
        	fmt.Fprintf(w, "ParseForm() err: %v", err)
        	return
    }

	if r.FormValue("username")!=""&&r.FormValue("password")!="" {

		if username==r.FormValue("username")&&password==r.FormValue("password"){
			message(w,"Giriş tamamlandı")
		}else{
			message(w,"Hatalı giriş")
		}

	}


}

func register(w http.ResponseWriter, r *http.Request)  {
	
	if r.Method=="GET"{
		
		htmlText(w,htmlRegisterText)
		
	}else if r.Method=="POST"{
		if err := r.ParseForm(); err != nil {
        	fmt.Fprintf(w, "ParseForm() err: %v", err)
        	return
    	}

		if r.FormValue("username")!=""&&r.FormValue("password")!="" {

			if username!=r.FormValue("username"){
				username=r.FormValue("username")
				password=r.FormValue("password")
				http.Redirect(w, r, "/register/kayit", http.StatusSeeOther)
			}else{
				htmlText(w,htmlRegisterText)
				message(w,"Hatalı kayıt")
			}


		}
	}
}
func registerkayit(w http.ResponseWriter, r *http.Request)  {
	
	htmlText(w,htmlRegisterkayitText)
		
}

func message(w http.ResponseWriter,text string){

	jsonResp, err := json.Marshal(text)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	w.Write(jsonResp)
}

func htmlText(w http.ResponseWriter,text string)  {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprint(w,text)
}