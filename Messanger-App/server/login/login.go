package login

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"server/mongodb"
	"server/register"

	"go.mongodb.org/mongo-driver/bson"
)

type RegisterData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	register.EnableCors(&w)

	body, _ := io.ReadAll(r.Body)
	defer r.Body.Close()

	var login RegisterData
	json.Unmarshal(body, &login)
	verioku := mongodb.DbVeriOkuma{
		DbName: "Users",
		DbColl: "Register",
		OkuVeri: bson.M{
			login.Username: login.Password,
		},
	}
	veri, err := verioku.VeriOku()
	yeniveri := string(veri)
	var jsonData map[string]interface{}
	errs := json.Unmarshal([]byte(yeniveri), &jsonData)
	if errs != nil {
		fmt.Println("JSON dönüşüm hatası:", errs)

	}

	if login.Username != "" && err == nil {

		w.Header().Set("Content-Type", "application/json")

		jsonData, err := json.Marshal(jsonData)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(jsonData)

	} else {
		w.Header().Set("Content-Type", "application/json")
		strData := `{"_id": ""}`
		var jsonData map[string]interface{}
		json.Unmarshal([]byte(strData), &jsonData)
		jsonDat, _ := json.Marshal(jsonData)

		w.Write(jsonDat)
	}

}
