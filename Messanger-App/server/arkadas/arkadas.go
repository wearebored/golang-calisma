package arkadas

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"server/acikbaglanti"
	"server/arkdb"
	"server/db"
	"server/register"
)

func Arkadas(w http.ResponseWriter, r *http.Request) {
	register.EnableCors(&w)
	body, _ := io.ReadAll(r.Body)
	defer r.Body.Close()
	var login arkdb.User
	json.Unmarshal(body, &login)
	fmt.Println(login)
	fmt.Printf("arkadas remode %v \n", r.RemoteAddr)
	fmt.Printf("arkadas remode %v \n", login)

	datas := arkdb.DataBase()
	mutex := arkdb.Mutex()

	acıkbag := acikbaglanti.BaglantiData()
	acıkbag.ConnectionsMutex.Lock()

	if _, ok := acıkbag.AdresDefteri[r.RemoteAddr]; ok {
		acıkbag.AdresDefteri[r.RemoteAddr] = acikbaglanti.Addres{
			Username: login.Username,
		}

	} else {
		acıkbag.AdresDefteri[r.RemoteAddr] = acikbaglanti.Addres{
			Username: login.Username,
			Kanal:    make(chan string),
		}
		acıkbag.AdresDefteri[r.RemoteAddr].Kanal <- "data"

	}
	acıkbag.ConnectionsMutex.Unlock()
	mutex.Lock()

	w.Header().Set("Content-Type", "application/json")

	fmt.Println(datas)
	veri := datas.DataBase[login.Username]
	err := json.NewEncoder(w).Encode(veri)

	if err != nil {
		log.Println("Error encoding JSON:", err)
	}

	mutex.Unlock()

}
func Arkadasekle(w http.ResponseWriter, r *http.Request) {
	register.EnableCors(&w)
	body, _ := io.ReadAll(r.Body)
	defer r.Body.Close()
	var login arkdb.Ekle
	json.Unmarshal(body, &login)

	datas1 := arkdb.DataBase()
	datas2 := db.DataBase()
	mutex1 := arkdb.Mutex()
	mutex2 := db.Mutex()
	fmt.Println(login)

	mutex1.Lock()
	mutex2.Lock()
	if login.Username != login.Ekle && datas2.DataBase[login.Ekle].Uid != "" {
		if _, ok := datas1.DataBase[login.Username]; !ok {
			datas1.DataBase[login.Username] = arkdb.ArkData{
				Arkadaslar: make(map[string]string),
			}
		}
		if _, ok := datas1.DataBase[login.Ekle]; !ok {
			datas1.DataBase[login.Ekle] = arkdb.ArkData{
				Arkadaslar: make(map[string]string),
			}
		}

		datas1.DataBase[login.Username].Arkadaslar[login.Ekle] = datas2.DataBase[login.Ekle].Uid
		datas1.DataBase[login.Ekle].Arkadaslar[login.Username] = datas2.DataBase[login.Username].Uid

		w.Header().Set("Content-Type", "application/json")

		veri := struct{ message string }{message: "Eklendi"}
		err := json.NewEncoder(w).Encode(veri.message)

		if err != nil {
			log.Println("Error encoding JSON:", err)
		}
	} else {
		w.Header().Set("Content-Type", "application/json")

		veri := struct{ message string }{message: "Eklenemedi"}
		err := json.NewEncoder(w).Encode(veri.message)

		if err != nil {
			log.Println("Error encoding JSON:", err)
		}
	}

	mutex2.Unlock()
	mutex1.Unlock()
	fmt.Printf("açık bağlantı 1 %v \n", acikbaglanti.BaglantiData())
	acikbaglanti.Send(login.Ekle, "arkekle")
	fmt.Println("nanay")
	acikbaglanti.Send(login.Username, "arkekle")
	// w.Header().Set("Content-Type", "application/json")

	// fmt.Println(datas)
	// veri := datas.DataBase[login.Username]
	// err := json.NewEncoder(w).Encode(veri)

	// if err != nil {
	// 	log.Println("Error encoding JSON:", err)
	// }
}
