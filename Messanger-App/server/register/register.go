package register

import (
	"encoding/json"
	"io"
	"net/http"
	"server/mongodb"

	"go.mongodb.org/mongo-driver/bson"
)

type RegisterData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Register(w http.ResponseWriter, r *http.Request) {

	EnableCors(&w)

	body, _ := io.ReadAll(r.Body)
	defer r.Body.Close()

	// --------gelen veri listeye çevrildi---------
	var register RegisterData
	json.Unmarshal(body, &register)

	// ------------------------
	verioku := mongodb.DbVeriOkuma{
		DbName: "Users",
		DbColl: "Register",
		OkuVeri: bson.M{
			register.Username: register.Password,
		},
	}
	veri, _ := verioku.VeriOku()

	if register.Username != "" && veri == nil {

		veriekle := mongodb.DbVeriYazma{
			DbName: "Users",
			DbColl: "Register",
			YazVeri: bson.M{
				register.Username: register.Password,
			},
		}
		_, err := veriekle.VeriEkle()
		if err != nil {
			w.Header().Set("Content-Type", "application/json")

			message := `eklenemedi`
			w.Write([]byte(message))
		} else {
			w.Header().Set("Content-Type", "application/json")

			message := `ok`
			w.Write([]byte(message))
		}

	} else {
		w.Header().Set("Content-Type", "application/json")

		message := `error`
		w.Write([]byte(message))
	}

}

func EnableCors(w *http.ResponseWriter) {
	headers := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization"
	methods := "GET, POST, PUT, DELETE, OPTIONS"
	origins := "http://localhost:3000"

	(*w).Header().Set("Access-Control-Allow-Origin", origins)
	(*w).Header().Set("Access-Control-Allow-Methods", methods)
	(*w).Header().Set("Access-Control-Allow-Headers", headers)
}
