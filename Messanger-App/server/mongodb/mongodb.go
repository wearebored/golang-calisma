// package main
package mongodb

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type VeriEkleOkuGuncelle interface {
	VeriEkle()
	VeriOku()
	// VeriGuncelle()
	VeriSil()
}

type DbVeriYazma struct {
	DbName  string
	DbColl  string
	YazVeri bson.M
}

type DbVeriOkuma struct {
	DbName  string
	DbColl  string
	OkuVeri bson.M
}

type DbVeriGuncelle1 struct {
	DbName  string
	DbColl  string
	OkuVeri bson.M
	YazVeri bson.M
}

type DbVeriGuncelle2 struct {
	DbName  string
	DbColl  string
	OkuVeri bson.M
	GunVeri string
	YazVeri string
}

type DbVeriSilme struct {
	DbName  string
	DbColl  string
	SilVeri bson.M
}

func dbBaglanti() (*mongo.Client, error) {
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	}

	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		return nil, errors.New("hata")
	}
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (veri *DbVeriYazma) VeriEkle() (string,error) {
	client, err := dbBaglanti()
	if err != nil {
		return "",err
	}
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	collection := client.Database(veri.DbName).Collection(veri.DbColl)

	_, err = collection.InsertOne(context.TODO(), veri.YazVeri)
	if err != nil {
		return "",err
	}

	fmt.Println("Yeni liste eklendi.")
	return "ok",nil
}

func (veri *DbVeriOkuma) VeriOku() ([]byte, error) {
	client, err := dbBaglanti()
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	coll := client.Database(veri.DbName).Collection(veri.DbColl)

	filter := veri.OkuVeri

	var result bson.M
	err = coll.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		return nil, err
	}

	fmt.Println("Sonuç:", result)

	jsonData, err := json.MarshalIndent(result, "", "")
	if err != nil {

		return nil, err
	}
	fmt.Printf("%s\n", jsonData)
	return jsonData, err
}

func VeriGuncelle(data interface{}) {
	// MongoDB'ye bağlanma kodu
	client, err := dbBaglanti()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	if veri, ok := data.(DbVeriGuncelle1); ok {
		// Veri1 türünde işlemler
		// Hedef koleksiyonu seçme
		coll := client.Database(veri.DbName).Collection(veri.DbColl)

		// Kullanıcıyı belirleme

		filter := veri.OkuVeri
		var key string
		for k := range filter {
			key = k
		}

		// Belgeyi sorgulama
		var result bson.M
		err = coll.FindOne(context.TODO(), filter).Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Sonuç:", result)

		// Yeni arkadaşı ekleme

		update := bson.M{"$set": bson.M{key: veri.YazVeri}}
		_, err = coll.UpdateOne(context.TODO(), filter, update)
		if err != nil {
			log.Fatal(err)
		}

		// Kullanıcı belgesini güncelleme

		fmt.Println("Yeni arkadaş eklendi.")
	} else if veri, ok := data.(DbVeriGuncelle2); ok {
		// Veri2 türünde işlemler
		// Hedef koleksiyonu seçme
		coll := client.Database(veri.DbName).Collection(veri.DbColl)

		// Kullanıcıyı belirleme

		filter := veri.OkuVeri

		// Belgeyi sorgulama
		var result bson.M
		err = coll.FindOne(context.TODO(), filter).Decode(&result)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Sonuç:", result)

		// Yeni arkadaşı ekleme

		update := bson.M{"$set": bson.M{veri.GunVeri: veri.YazVeri}}
		_, err = coll.UpdateOne(context.TODO(), filter, update)
		if err != nil {
			log.Fatal(err)
		}

		// Kullanıcı belgesini güncelleme

		fmt.Println("Yeni arkadaş eklendi.")

	} else {
		fmt.Println("Geçersiz veri türü")
	}

}

func (veri *DbVeriSilme) VeriSil() {
	// MongoDB'ye bağlanma kodu
	client, err := dbBaglanti()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	// Hedef koleksiyonu seçme
	coll := client.Database(veri.DbName).Collection(veri.DbColl)

	filter := veri.SilVeri
	// Belgeyi silme
	result, err := coll.DeleteOne(context.TODO(), filter)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Silinen belge sayısı:", result.DeletedCount)

}

func main() {
// 	veri := &DbVeriOkuma{
// 		DbName: "Users",
// 		DbColl: "Register",
// 		OkuVeri: bson.M{
// 			"register.Username": "register.Password",
// 		},
// 	}

// 	ver, err := veri.VeriOku()
// 	fmt.Printf("hata %e \n", err)
// 	fmt.Printf("hata %o \n", ver)

	// veri := DbVeriYazma{
	// 	DbName: "Users",
	// 	DbColl: "Register",
	// 	YazVeri: bson.M{
	// 		"register.Username":"register.Password",
	// 	},
	// }
	// ver,err:=veri.VeriEkle()
	// fmt.Printf("hata %e \n", err)
	// fmt.Printf("hata %s \n", ver)
// 	// veri := DbVeriGuncelle1{
// 	// 	DbName: "myChat",
// 	// 	DbColl: "myRegister",
// 	// 	OkuVeri: bson.M{
// 	// 		"dates": "1234",
// 	// 	},
// 	// 	YazVeri: bson.M{
// 	// 		"vera": "dwad",
// 	// 		"adwwad":"dawdw",
// 	// 		"dawa":bson.A{"daw","dwad"},
// 	// 	},
// 	// }

// 	// veri := DbVeriGuncelle2{
// 	// 	DbName: "myChat",
// 	// 	DbColl: "myRegister",
// 	// 	OkuVeri: bson.M{
// 	// 		"sonekle": "tamam" ,
// 	// 	},
// 	// 	GunVeri: "dates.dawa[0]",
// 	// 	YazVeri: "",
// 	// }
// 	// VeriGuncelle(veri)

// 	// veri := DbVeriSilme{
// 	// 	DbName: "myChat",
// 	// 	DbColl: "myRegister",
// 	// 	SilVeri: bson.M{
// 	// 		"ekelme":  "2.ekelme",
// 	// 	},
// 	// }
// 	// veri.VeriSil()
}
