package arkdb

import "sync"

type User struct {
	Username string `json:"name"`
	Uid      string `json:"uid"`
}
type Ekle struct {
	Username string `json:"name"`
	Uid      string `json:"uid"`
	Ekle     string `json:"ekle"`
}
type ArkData struct {
	Arkadaslar map[string]string
}

type RegisterDataBase struct {
	DataBase map[string]ArkData
	mutex    sync.RWMutex
}
type Message struct {
	message string
}

var datas = RegisterDataBase{
	DataBase: make(map[string]ArkData),
	mutex:    sync.RWMutex{},
}


func DataBase() *RegisterDataBase {
	
	return &datas

}
func Mutex() *sync.RWMutex {
	return &datas.mutex
}
