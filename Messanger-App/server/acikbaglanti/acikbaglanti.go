package acikbaglanti

import (
	"context"
	"fmt"
	"net/http"
	"sync"
)

type Connection struct {
	RemoteAddr string
	Writer     http.ResponseWriter
	Flusher    http.Flusher
	RequestCtx context.Context
}

type ConnectionData struct {
	Connections      map[string]Connection
	ConnectionsMutex sync.RWMutex
	AdresDefteri     map[string]Addres
}

type Addres struct {
	Username string
	Kanal    chan string
}

var data = ConnectionData{
	Connections:      make(map[string]Connection),
	ConnectionsMutex: sync.RWMutex{},
	AdresDefteri:     make(map[string]Addres),
}

func BaglantiData() *ConnectionData {
	return &data
}

func RemoveConnection(client string) {
	datas := BaglantiData()
	datas.ConnectionsMutex.Lock()
	defer datas.ConnectionsMutex.Unlock()

	delete(datas.Connections, client)
}

func Send(user string, msg string) {
	datas := BaglantiData()
	datas.ConnectionsMutex.RLock()
	defer datas.ConnectionsMutex.RUnlock()

	fmt.Printf("Açık bağlantı: %v\n", datas.Connections[user])
	msgBytes := []byte("event: message\n\ndata:" + msg + "\n\n")

	if conn, ok := datas.Connections[user]; ok {
		_, err := conn.Writer.Write(msgBytes)
		if err != nil {
			fmt.Println("Kullanıcıya gönderme hatası:", err)
			RemoveConnection(user)
		}
		if conn.Flusher != nil {
			conn.Flusher.Flush()
		}
	}
}
