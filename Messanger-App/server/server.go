package main

import (
	"fmt"
	"log"
	"net/http"

	"server/acikbaglanti"
	"server/arkadas"
	"server/login"
	"server/register"
)

func main() {

	http.HandleFunc("/", handler)
	http.HandleFunc("/register", register.Register)
	http.HandleFunc("/login", login.Login)
	http.HandleFunc("/arkadas", arkadas.Arkadas)
	http.HandleFunc("/arkadasekle", arkadas.Arkadasekle)
	http.HandleFunc("/baglanti", Baglan)

	certFile := "localhost.crt"
	keyFile := "localhost.key"
	log.Fatal(http.ListenAndServeTLS(":3001", certFile, keyFile, nil))

}
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Merhaba, HTTPS dünyası!")
}

func Baglan(w http.ResponseWriter, r *http.Request) {

	register.EnableCors(&w)

	fmt.Printf("bağlanti remode %v \n", r.RemoteAddr)

	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}
	mutex := acikbaglanti.BaglantiData()
	data := acikbaglanti.BaglantiData().Connections
	requestContext := r.Context()
	mutex.ConnectionsMutex.Lock()
	if _, ok := mutex.AdresDefteri[r.RemoteAddr]; !ok {
		mutex.AdresDefteri[r.RemoteAddr] = acikbaglanti.Addres{
			Username: "",
			Kanal:    make(chan string),
		}
		mutex.ConnectionsMutex.Unlock()
		<-mutex.AdresDefteri[r.RemoteAddr].Kanal
		mutex.ConnectionsMutex.Lock()
		user := mutex.AdresDefteri[r.RemoteAddr].Username
		fmt.Printf("bağlanti remode %v \n", user)
		data[user] = acikbaglanti.Connection{
			RemoteAddr: r.RemoteAddr,
			Writer:     w,
			Flusher:    flusher,
			RequestCtx: requestContext,
		}

	} else {
		user := mutex.AdresDefteri[r.RemoteAddr].Username
		fmt.Printf("bağlanti remode %v \n", user)
		data[user] = acikbaglanti.Connection{
			RemoteAddr: r.RemoteAddr,
			Writer:     w,
			Flusher:    flusher,
			RequestCtx: requestContext,
		}

	}

	defer func() {
		acikbaglanti.RemoveConnection(mutex.AdresDefteri[r.RemoteAddr].Username)
	}()
	mutex.ConnectionsMutex.Unlock()
	// w.Header().Set("Content-Type", "application/json")

	// json.NewEncoder(w).Encode(mutex.Connections)

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	<-requestContext.Done()
	fmt.Println("sonlandı")

}

// func removeConnection( client string) {
// 	datas := acikbaglanti.BaglantiData()
// 	datas.ConnectionsMutex.Lock()
// 	defer datas.ConnectionsMutex.Unlock()

// 	delete(datas.Connections, client)
// }

// func  Send( user string, msg string) {
// 	datas := acikbaglanti.BaglantiData()
// 	datas.ConnectionsMutex.RLock()

// 	defer datas.ConnectionsMutex.RUnlock()

// 	msgBytes := []byte("event: message\n\ndata:" + msg + "\n\n")

// 	fmt.Println("Post Kullanıcı: " + user)

// 	for client, connection := range datas.Connections {
// 		fmt.Println("Kullanıcı :" +client)

// 		if _, ok := datas.Connections[user]; ok {
// 			_, err := connection.Writer.Write(msgBytes)

// 			if err != nil {
// 				removeConnection(user)
// 				continue
// 			}
// 			connection.Flusher.Flush()
// 		}

// 	}
// }
