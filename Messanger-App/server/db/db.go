package db

import "sync"

type User struct {
	Username string `json:"name"`
	Uid      string `json:"uid"`
}
type RegisterData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
type RegisterUid struct {
	RegisterData
	Uid string
}
type RegisterDataBase struct {
	DataBase map[string]RegisterUid
	mutex    sync.RWMutex
}

var datas = RegisterDataBase{
	DataBase: make(map[string]RegisterUid),
	mutex:    sync.RWMutex{},
}

func DataBase() *RegisterDataBase {

	return &datas

}
func Mutex() *sync.RWMutex {
	return &datas.mutex
}
