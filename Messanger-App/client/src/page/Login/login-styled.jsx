import styled from "styled-components";

export const LoginRegisterContainer = styled.div`
  min-height: 92vh;
  min-width: min-content;
  padding: 100px;
`;

export const LoginRegister = styled.div`
  background-color: #000000b5;
  height: 100%;
  width: 100%;
  border-radius: 20px;
  padding: 100px;
  color: white;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 50px;
  h3 {
    font-size: 3rem;
    color: #ffffff;
  }
  form {
    display: flex;
    flex-direction: column;
    align-items: center;

    gap: 10px;

    label {
      font-size: 1.5rem;
      color: #7fff17;
      height: 1.7rem;
    }
    input {
      height: 40px;
      width: 300px;
      border-radius: 10px;
      padding: 2px 10px;
      font-size: 1.2rem;
    }
    button {
      height: 40px;
      width: 300px;
      border-radius: 10px;
      font-size: 1.5rem;
      margin-top: 20px;
      background-color: #1af0ff;
    }
    #hata {
      color: red;
      margin: 30px 0 -40px 0;
    }
  }
`;
