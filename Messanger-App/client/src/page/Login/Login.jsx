import React, { useState } from "react";
import { LoginRegisterContainer, LoginRegister } from "./login-styled";
import { useDispatch, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";
import { setLogin } from "../../app/features/LoginSlice";

function Login() {
  const { user } = useSelector((s) => s.login);
  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const url = "https://localhost:3001/login";
  var ButtonClick = async () => {
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });
      const data = await response.json();
      console.log(data);
      if (data._id !== "") {
        dispatch(
          setLogin({
            user: username,
            uid: data._id,
          })
        );
      } else {
        setMessage("Girdiğiniz Bilgiler Hatalı");
      }
      console.log(data);
    } catch (error) {
      console.error("Error:", error);
    }
    setUsername("");
    setPassword("");
  };

  var UserChange = (e) => {
    setUsername(e.target.value);
  };
  var PasswordChange = (e) => {
    setPassword(e.target.value);
  };

  if (user === "") {
    return (
      <LoginRegisterContainer>
        <LoginRegister>
          <h3>Login</h3>
          <form action="">
            <label htmlFor="">Username</label>
            <input
              onChange={UserChange}
              value={username}
              type="text"
              name="username"
              id="username"
            />
            <label htmlFor="">Password</label>
            <input
              onChange={PasswordChange}
              value={password}
              type="password"
              name="password"
              id="password"
            />
            <button onClick={ButtonClick} type="button">
              Login
            </button>
            <label id="hata" htmlFor="">
              {message}
            </label>
          </form>
        </LoginRegister>
      </LoginRegisterContainer>
    );
  } else {
    return <Navigate to="/chat" />;
  }
}

export default Login;
