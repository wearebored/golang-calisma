import styled from "styled-components";

export const ChatContainer = styled.div`
  height: 92vh;
  /* min-width: min-content; */
  /* padding: 100px; */
`;

export const Kullanıcılar = styled.div`
  background-color: red;
  width: 400px;
  height: 92vh;
  position: fixed;
  left: 0;
  top: 8vh;
  padding: 10px;
  display: flex;
  flex-direction: column;
  gap: 10px;
`;
export const Kisiler = styled.div`
  overflow-y: scroll;
  overflow-x: hidden;
  background-color: #b7f1ff;
  width: 100%;
  height: 100%;
  padding: 10px;
 

  &::-webkit-scrollbar {
    width: 10px;
  }

  &::-webkit-scrollbar-track {
  }

  &::-webkit-scrollbar-thumb {
    background-color: #adadad;
    border-radius: 5px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background-color: #b8b5b5;
  }
`;

export const UserAdd = styled.div`
  background-color: green;
  width: 100%;
  height: 200px;
  div {
    width: 100%;
    height: 50%;
    border: solid 2px yellow;
    background-color: #695f5f;
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 5px;
    input {
      height: 35px;
      border-radius: 5px;
      padding-left: 5px;
    }
    button {
      height: 35px;
      width: 110px;
      border-radius: 5px;
    }
  }
`;

export const MessageOneri = styled.div`
  margin-left: 400px;
  width: calc(100vw - 400px);
  padding: 20px;
  overflow-y: scroll;
  overflow-x: hidden;
  height: 92vh;
  position: relative;
  
  
  &::-webkit-scrollbar {
    width: 10px;
  }

  &::-webkit-scrollbar-track {
    
  }

  &::-webkit-scrollbar-thumb {
    background-color: #adadad;
    border-radius: 5px;
  }

  &::-webkit-scrollbar-thumb:hover {
    background-color: #b8b5b5;
  }
`;
