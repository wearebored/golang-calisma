import React, { useEffect, useState } from "react";
import {
  ChatContainer,
  Kisiler,
  Kullanıcılar,
  MessageOneri,
  UserAdd,
} from "./chat-styled";
import Users from "../../components/Users/Users";
import Mesajlar from "../../components/Mesajlar/Mesajlar";
import Onerilenler from "../../components/Onerilenler/Onerilenler";
import { useSelector } from "react-redux";

function Chat() {
  const { user, uid } = useSelector((s) => s.login);
  const [arkadas, setArkadas] = useState("");
  const [ekleuser, setEkleuser] = useState("");
  const [chatoneri, setChatoneri] = useState("0");
  const [username, setUsername] = useState("");
  const [baglanti, setBaglanti] = useState("0");

  const url = "https://localhost:3001/arkadas";
  var kullanıcılar = async () => {
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: user,
          uid: uid,
        }),
      });
      const data = await response.json();
      setArkadas(data.Arkadaslar);
      console.log(data.Arkadaslar);
    } catch (error) {
      console.error("Error:", error);
    }
  };
  var kullanıcıekle = async () => {
    try {
      const response = await fetch(url + "ekle", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: user,
          uid: uid,
          ekle: ekleuser,
        }),
      });
      const data = await response.json();

      console.log(data);
    } catch (error) {
      console.error("Error:", error);
    }
  };
  var ekleChange = (e) => {
    setEkleuser(e.target.value);
  };
  var ekleClick = () => {
    kullanıcıekle();
    setEkleuser("");
  };
  var geri = () => {
    onkeydown = (e) =>
      e.key === "Escape" ? setChatoneri("0") : setEkleuser(e.target.value);
  };

  var acıkbaglanti = () => {
    var source = new EventSource("https://localhost:3001/baglanti");

    source.onopen = function (event) {
      console.log("connected", event);
    };

    source.onerror = function (event) {
      console.log("error", event);
    };

    source.onmessage = function (event) {
      console.log(event.data);
    };
  };

  useEffect(() => {
    geri();
    kullanıcılar();
    if (baglanti === "0") {
      acıkbaglanti();
      setBaglanti("1");
    }
  }, []);

  return (
    <ChatContainer>
      <Kullanıcılar>
        <Kisiler>
          {arkadas &&
            Object.keys(arkadas).map((e, index) => (
              <Users
                key={index}
                id={e}
                veri={arkadas[e]}
                click={setChatoneri}
                username={setUsername}
              />
            ))}
        </Kisiler>
        <UserAdd>
          <div>
            <input
              onChange={ekleChange}
              value={ekleuser}
              type="text"
              name="ekle"
              id="ekle"
              placeholder="Username Giriniz..."
            />
            <button onClick={ekleClick}>Kulancıyı Ekle</button>
          </div>
          <div>
            <button onClick={() => setChatoneri("2")}>
              Önerilen Kullanıcılar
            </button>
          </div>
        </UserAdd>
      </Kullanıcılar>
      <MessageOneri>
        {chatoneri === "0" ? (
          <div>giriş sayfası</div>
        ) : chatoneri === "1" ? (
          <Mesajlar username={username} />
        ) : (
          chatoneri === "2" && <Onerilenler />
        )}
      </MessageOneri>
    </ChatContainer>
  );
}

export default Chat;
