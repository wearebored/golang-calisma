import React, { useState } from "react";
import { LoginRegisterContainer, LoginRegister } from "../Login/login-styled";
import { useSelector } from "react-redux";
import { Navigate, useNavigate } from "react-router-dom";

function Register() {
  const { user } = useSelector((s) => s.login);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const url = "https://localhost:3001/register";
  const navigate = useNavigate();
  var ButtonClick = async () => {
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });

      if (response.ok) {
        const responseData = await response.text();
        responseData === "ok"
          ? setMessage("Kaydınız Tamamlandı")
          : setMessage("Kullanıcı Adı Kullanılmaktadır");
        if (responseData === "ok") {
          setTimeout(() => {
            navigate("/login");
          }, 1000);
        }
      } else {
        console.error("Error:", response.status);
      }
    } catch (error) {
      console.error("Error:", error);
    }
    setUsername("");
    setPassword("");
  };

  var UserChange = (e) => {
    setUsername(e.target.value);
   
  };
  var PasswordChange = (e) => {
    setPassword(e.target.value);
   
  };

  if (user === "") {
    return (
      <LoginRegisterContainer>
        <LoginRegister>
          <h3>Register</h3>
          <form action="">
            <label htmlFor="">Username</label>
            <input
              onChange={UserChange}
              value={username}
              type="text"
              name="username"
              id="username"
            />
            <label htmlFor="">Password</label>
            <input
              onChange={PasswordChange}
              value={password}
              type="password"
              name="password"
              id="password"
            />
            <button onClick={ButtonClick} type="button">
              Register
            </button>
            <label id="hata" htmlFor="">
              {message}
            </label>
          </form>
        </LoginRegister>
      </LoginRegisterContainer>
    );
  } else {
    return <Navigate to="/chat" />;
  }
}

export default Register;
