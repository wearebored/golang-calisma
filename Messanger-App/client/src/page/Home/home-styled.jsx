import styled from "styled-components";

export const HomeContainer = styled.div`
  /* background-image: url("images/320986.jpg");
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center; */
  min-height: 92vh;
  width: 100%;
  padding: 100px;
`;

export const HomeMetin = styled.div`
  background-color: #000000b5;
  height: 100%;
  width: 100%;
  border-radius: 20px;
  padding: 100px;
  color: white;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 50px;
  h3 {
    font-size: 3rem;
    color: #ffffff;
  }
  p{
    font-size: 1.2rem;
  }
  
`;
