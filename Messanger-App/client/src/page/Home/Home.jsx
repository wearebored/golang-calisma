import React from "react";
import { HomeContainer, HomeMetin } from "./home-styled";
import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

function Home() {
  const {user}=useSelector((s)=>s.login)
  if(user===""){
    return (
      <HomeContainer>
        <HomeMetin>
          <h3>Hoş Geldiniz!</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque
            corrupti similique hic, repellendus ex quos eos, alias labore
            laudantium quod qui deleniti porro! Iure provident aliquam voluptas
            harum et sunt incidunt, cum pariatur quam quibusdam, ipsa minima
            temporibus! Qui quisquam incidunt maxime deserunt, unde optio
            possimus necessitatibus sapiente alias consequuntur sunt ratione
            quia aut quibusdam aperiam perspiciatis vero, at eius sint
            voluptatum ducimus consequatur! Quod veniam eum nobis at rem, cum
            quo provident laboriosam iusto magni saepe laborum deserunt odit
            earum voluptatibus ad, suscipit nemo. Fugiat nisi id facilis qui
            optio cupiditate sunt, perspiciatis impedit non illo dolor eos
            veniam.
          </p>
        </HomeMetin>
      </HomeContainer>
    );
  }else{
    
    return <Navigate to="/chat" />;
  }
  
}

export default Home;
