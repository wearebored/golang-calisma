import React from "react";
import { LogReg, NavbarContainer } from "./navbar-styled";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function Navbar() {
  const { user } = useSelector((s) => s.login);
  return (
    <NavbarContainer>
      <Link id="baslik" to="/">
        <h1>BoredChat!</h1>
      </Link>
      <LogReg>
        {user === "" && (
          <Link id="link" to="/login">
            <h2>Login</h2>
          </Link>
        )}
        {user === "" && (
          <Link id="link" to="/register">
            <h2>Register</h2>
          </Link>
        )}
        {user !== "" && <h2 id="link">Merhaba {user}</h2>}
      </LogReg>
    </NavbarContainer>
  );
}

export default Navbar;
