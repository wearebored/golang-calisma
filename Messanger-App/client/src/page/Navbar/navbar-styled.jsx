import styled from "styled-components";

export const NavbarContainer = styled.div`
  background-color: #a3ffa3;
  height: 8vh;
  width: 100%;

  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 60px;
  h1 {
    font-size: 3rem;
  }
  #link {
    text-decoration: none;
    color: #1150ff;
  }
  #baslik {
    text-decoration: none;
    color: #000000;
  }
  position: fixed;
  left: 0;
  top: 0;
  
`;

export const LogReg = styled.div`
  display: flex;
  gap: 30px;
`;
