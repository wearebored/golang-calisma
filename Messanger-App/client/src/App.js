import { store } from "./app/store";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Navbar from "./page/Navbar/Navbar";
import Home from "./page/Home/Home";
import Login from "./page/Login/Login";
import Register from "./page/Register/Register";
import { Provider } from "react-redux";
import RoutePrivate from "./private/RoutePrivate";
import Chat from "./page/Chat/Chat";

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Navbar />

          <Routes>
            <Route element={<RoutePrivate />}>
              <Route path="/chat" element={<Chat />} />
            </Route>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
