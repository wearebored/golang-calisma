import React from "react";
import { MesajCon } from "./mesajlar-styled";
import MyMessage from "../Messages/MyMessage";
import YourMessage from "../Messages/YourMessage";

function MesajContainer() {
  let mesaj = true;

  return <MesajCon>{mesaj ? <MyMessage /> : <YourMessage />}</MesajCon>;
}

export default MesajContainer;
