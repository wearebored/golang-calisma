import styled from "styled-components";

export const MesajlarContainer = styled.div`
  border: solid 2px red;
  padding: 2px;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  gap: 10px;
  padding-bottom: 50px;
`;
export const MesajCon = styled.div`
  border: solid 2px black;
  padding: 2px;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  gap: 10px;
  position: relative;
  

`;

export const CevapContainer = styled.div`
  height: 60px;
  width: calc(100vw - 400px);
  background-color: #bffcbf;
  position: fixed;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  gap: 10px;
  padding: 5px 20px;
  input {
    width: 100%;
    border-radius: 5px;
    height: 50px;
    padding-left: 10px;
    font-size: 1.2rem;
  }
  button {
    border-radius: 10px;
    padding: 10px;
    font-size: 1rem;
  }
`;
