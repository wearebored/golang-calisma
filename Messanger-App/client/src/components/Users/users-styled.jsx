import styled from "styled-components";

export const UsersContainer = styled.div`
  height: 100px;
  border: solid 2px;
  padding: 2px;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`;
