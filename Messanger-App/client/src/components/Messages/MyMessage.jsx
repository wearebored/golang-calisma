import React from 'react'
import { MyMessageContainer } from './messages-styled'

function MyMessage() {
  return (
    <MyMessageContainer>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio hic a odit eius id alias accusamus impedit eaque, repellat voluptatum!</p>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam eum dolor voluptas eligendi temporibus recusandae nostrum laborum delectus non aut nam hic, at, explicabo, nesciunt totam quod accusantium magnam! In?</p>
    </MyMessageContainer>
  )
}

export default MyMessage