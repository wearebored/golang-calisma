import styled from "styled-components";

export const UsersContainer = styled.div`
  height: 100px;
  border: solid 2px;
  padding: 2px;
`;

export const MyMessageContainer = styled.div`
  width: 50%;
  border: solid 2px green;
  padding: 10px;
  border-radius: 10px;
  background-color: #e0f6ff;
`;
export const YourMessageContainer = styled.div`
  width: 50%;
  border: solid 2px green;
  padding: 10px;
  border-radius: 10px;
  margin-left: 50%;
  background-color: #d7ffd5;
`;
