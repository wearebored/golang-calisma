import React from 'react'
import { YourMessageContainer } from './messages-styled'

function YourMessage() {
  return (
    <YourMessageContainer>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eum explicabo in adipisci facere dolor excepturi obcaecati, delectus quae quisquam libero eligendi fugit sequi veritatis, optio dolorum! Asperiores alias tempore exercitationem?</p>
    </YourMessageContainer>
  )
}

export default YourMessage