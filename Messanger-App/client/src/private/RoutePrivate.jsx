import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

function RoutePrivate() {
  const { user } = useSelector((store) => store.login);

  return user ? <Outlet /> : <Navigate to="/login" />;
}

export default RoutePrivate;
