import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: "",
  uid: "",
};

const LoginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.user = action.payload.user;
      state.uid = action.payload.uid;
    },
    setLogout: (state, action) => {
      state.user = "";
      state.uid = "";
    },
    setLoginData: (state, action) => {},
  },
});

export const { setLogin, setLogout, setLoginData } = LoginSlice.actions;

export default LoginSlice.reducer;
